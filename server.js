var bankai = require('bankai')
var concat = require('concat-stream')
var body = require('body-parser')
var pump = require('pump')
var fs = require('fs')
var path = require('path')
var express = require('express')
var index = require('./pages/index')
var app = require('./app')
var Paps = require('./pap')
var level = require('level-party')
var sub = require('subleveldown')

var db = level('./database')
var counts = sub(db, 'counts')
var paps = Paps(sub(db, 'paps', {valueEncoding: 'json'}))

var server = express()
server.use('/static', express.static('static'))
server.use(body.urlencoded({extended: false}))

var transforms = {css: {use: ['sheetify-nested']}}
var assets = bankai(path.join(__dirname, 'app.js'), transforms)

function renderHTML (req, res, state) {
  if (!state) state = {}
  var content = app.toString(req.url, Object.freeze(state))
  res.setHeader('Content-Type', 'text/html')
  res.end(index(content, state))
}

server.get('/', function (req, res)  {
  paps.latest(function (err, pap) {
    if (err) return onerror(err, res)
    renderHTML(req, res, {pap: pap, id: pap.id})
  })
})

server.get('/bundle.js', function (req, res)  {
  return assets.js(req, res).pipe(res)
})

server.get('/punch/:id', function (req, res)  {
  paps.punch(parseInt(req.params.id), function (err, pap) {
    if (err) return onerror(err, res)
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify(pap))
  })
})

server.get('/admin/create', function (req, res)  {
  renderHTML(req, res)
})

server.post('/admin/create', function (req, res)  {
  paps.create(req.body, function (err) {
    if (err) return onerror(err, res)
    res.end('done!')
  })
})

function onerror (err, res) {
  console.trace(err)
  return res.json({"error": err.message})
}

server.get('/admin/edit/:id', function (req, res)  {
  paps.get(req.params.id, function (err, pap) {
    if (err) return onerror(err, res)
    console.log(pap)
    renderHTML(req, res, {pap: pap})
  })
})

server.post('/admin/edit/:id', function (req, res)  {
  paps.update(req.params.id, req.body, function (err, pap) {
    if (err) return onerror(err, res)
    res.end(JSON.stringify(pap))
  })
})

server.get('/pap/:id', function (req, res)  {
  paps.get(req.params.id, function (err, pap) {
    res.end(JSON.stringify(pap))
  })
})

server.get('/punch/:id', function (req, res)  {
  var key = req.params.id
  counts.get(key, function (err, count) {
    if (err)  {
      if (err.notFound) count = 1
      else return onerror(err, res)
    }
    counts.put(key, parseInt(count) + 1, function (err) {
      if (err) return onerror(err, res)
      counts.get(key, function (err, count) {
        if (err) return onerror(err, res)
        res.send(200, count)
      })
    })
  })
})

server.get('/bundle.css', function (req, res)  {
  return assets.css(req, res).pipe(res)
})

var port = process.env.PORT || 8001

server.listen(port, function () {
  console.log('listening on http://localhost:' + port)
})
