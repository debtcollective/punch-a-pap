var html = require('choo/html')
var css = require('sheetify')
var renderPap = require('./pap')

module.exports = function (state, emit) {
  state.paps = module.parent ? null : window._state.paps
  if (!state.paps) {
    return html`<a href="/create">there are no paps. create your first pap</a>`
  }
  var className = css`
    :host {
      img.pap {
        width: 300px;
      }
    }
  `
  return html`<div class="${className}">
    ${state.paps.map(function (pap) {
      return renderPap({pap: pap})
    })}
  </div>`
}
