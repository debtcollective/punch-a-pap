var html = require('choo/html')
var css = require('sheetify')
var pap = require('./pap')

module.exports = function (state, emit) {
  css('tachyons')
  var prefix = css`
    :host {
      background-color:black;
      color: #eee;
      max-height: 100%;
    }
  `
  return html`<div class=${prefix}>
    ${pap(state, emit)}
  </div>
  `

}
