var html = require('choo/html')
var xhr = require('xhr')
var css = require('sheetify')

module.exports = function (state, emit) {
  state.pap = module.parent ? null : window._state.pap
  if (!state.pap) {
    return html`<a href="/admin/create">create your first pap</a>`
  }

  var prefix = css`
    :host {
      display: flex;
      flex-direction: column;
      max-height: 100%;

      img.pap {
        height: 100%;
      }
      #bubble {
        display: none;
      }
      footer {
        display: flex;
        height: 50px;
        width: 100%;
        background-color: black;
        align-items: center;
        justify-content: space-between;
        color: #eee;
        .name {
          padding-left: 10px;
        }
        .count {
          padding-right: 10px;
        }
      }
    }
  `
  //<h3><textarea type="text" rows=10 cols=50 placeholder="Why are you punching?"></textarea></h3>
  return html`<div class="${prefix}">
    <header>
      <div>Punch a Pro Austerity Politician</div>
    </header>
    <img id="bubble" src="static/wham.png">
    <img class="pap" src="${state.pap.url}" onclick=${onclick} />
    <footer>
      <h4 class="name">${state.pap.name}</h4>
      <div class="count">${state.pap.count} punches</div>
    </footer>
  </div>`

  function onclick (event) {
    var image_bubble = document.querySelector('#bubble')
    var x = event.clientX
    var y = event.clientY
    image_bubble.style.position = "absolute"
    console.log(x)
    console.log(y)
    image_bubble.style.top = y + "px"
    image_bubble.style.left = x + "px"
    var opacity = 1;  // initial opacity
    image_bubble.style.opacity = opacity;
    image_bubble.style.display = 'block';

    image_bubble.style.filter = 'alpha(opacity=' + opacity * 100 + ")";
    var timer = setInterval(function () {
        if (opacity <= 0.1){
          clearInterval(timer);
          image_bubble.style.display = 'none';
          emit('punch', {id: state.pap.id})
        }
        image_bubble.style.opacity = opacity;
        image_bubble.style.filter = 'alpha(opacity=' + opacity * 100 + ")";
        opacity -= opacity * 0.1;
    }, 50);

    image_bubble.animate([
      { transform: 'translateY(0px)' },
      { transform: 'translateY(-300px)' }
    ], {
      duration: 1000,
      iterations: Infinity
    });
    console.log('punching!!!')
  }
}
