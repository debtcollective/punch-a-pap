var html = require('choo/html')
var xtend = require('xtend')
var css = require('sheetify')

module.exports = function (state, emit) {
  state.pap = module.parent ? null : window._state.pap
  var pap = xtend({name: '', url: '', description: ''}, state.pap)
  var prefix = css`
    :host {
      form {
        display: flex;
        flex-direction: column;
        width: 100%;
        max-width: 400px;
      }

      input {
        width: 100%;
      }
    }
  `
  return html`<div class="${prefix}">
    <form method="POST">
      <input name="name" type="text" value="${pap.name}" placeholder="name" />
      <input name="url" type="text" value="${pap.url}" placeholder="URL" />
      <textarea name="description"  type="image" placeholder="Description">${pap.description}</textarea>
      <input type="Submit" value="Submit" />
    </form>
  </div>
  `

}
