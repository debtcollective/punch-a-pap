var serializeJS = require('serialize-javascript')
var html = require('choo/html')

module.exports = function (content, state) {
  return `
  <html lang="en" dir="ltr">
    <head>
      <meta charset="utf-8">
      <link rel="stylesheet" href="/bundle.css">
      <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
      <div id="content"></div>
    </body>
    <script type="text/javascript">
      window._state = ${serializeJS(state)}
    </script>
    <script src="/bundle.js"></script>
    </body>
  </html>
  `

}
