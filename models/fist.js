var xhr = require('xhr')

module.exports = function (state, emitter) {
  state.count = module.parent ? 0 : window._state.count
  emitter.on('DOMContentLoaded', function () {
    var timer = setInterval(function () {
      if (typeof state.view === 'undefined') return clearInterval(timer)
      xhr('/pap/' + state.view, {json: true}, function (err, resp, json) {
        if (err) return console.trace(err)
        state.pap.count = json.count
        emitter.emit('render')
      })
    }, 1000)
  })
  emitter.on('punch', function (data) {
    if (state.punching) return
    state.punching = true
    xhr('/punch/' + data.id, {json: true}, function (err, resp, json) {
      if (err) return console.trace(err)
      state.pap.count = json.count
      state.punching = false
      emitter.emit('render')
    })
  })
}
