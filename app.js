var choo = require('choo')

var app = choo()

app.use(require('./models/fist'))
app.route('/', require('./pages/main'))
app.route('/punch/:id', require('./pages/main'))
app.route('/admin/create', require('./pages/create'))
app.route('/admin/edit/:id', require('./pages/create'))
app.route('/paps', require('./pages/paps'))

if (module.parent) {
  module.exports = app
} else {
  app.mount('#content')
}
