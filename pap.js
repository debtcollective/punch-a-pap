var xtend = require('xtend')

module.exports = function pap (paps) {
  return {
    punch, update, get, create, latest
  }

  function punch (id, cb) {
    paps.get(parseInt(id),  function (err, pap) {
      if (err) return cb(err)
      pap.count += 1
      paps.put(pap.id, pap, function (err) {
        if (err) return cb(err)
        return cb(null, pap)
      })
    })
  }

  function update (id, data, cb) {
    get(id, function (err, pap) {
      if (err) return cb(err)
      var edited = xtend(pap, data)
      paps.put(id, edited, function (err) {
        if (err) return cb(err)
        return cb(null, edited)
      })
    })
  }

  function get (id, cb) {
    paps.get(parseInt(id), function (err, pap) {
      if (err) return cb(err)
      return cb(null, pap)
    })
  }

  function create (data, cb) {
    paps.get('length', function (err, length) {
      if (err) return cb(err)
      length = parseInt(length)
      console.log('creating a new pap with data', length, data)
      data.count = 1
      data.id = length
      paps.put(length, data, function (err) {
        if (err) return cb(err)
        paps.put('length', length + 1, cb)
      })
    })
  }

  function latest (cb) {
    paps.get('length', function (err, length) {
      if (err) return paps.put('length', 0, cb)
      length = parseInt(length)
      if (length === 0) return cb()
      console.log(length)
      paps.get(length - 1,  cb)
    })
  }
}
